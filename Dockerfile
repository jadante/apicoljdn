#Imagen inicial desde la q se crea la nuestra
FROM node
#en que dir de docker se va aguardar nuestra api
WORKDIR /colapidb_jdn
#anadir del directorio actual al que acabamos de crear
ADD . /colapidb_jdn
#puerto para escucha (el que se ha definido en la api)
EXPOSE 3000
#comandos para lanzar nuestra api rest colapijdn
CMD ["npm", "start"]
#CMD ["node", "server.js"]-- (para cuando no usemos nodemon)
